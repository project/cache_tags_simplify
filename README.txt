Cache tags simplify

See https://www.drupal.org/project/drupal/issues/3001276 and https://www.drupal.org/project/purge/issues/2952277 to
understand why this module can be useful.