<?php

namespace Drupal\cache_tags_simplify\EventSubscriber;

use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Simplify the list of cache tags that will be put in the HTTP headers.
 *
 * Simplification aims at reducing redundant cache tags from the list of
 * tags. For example we have a view that shows 5 nodes and adds all of their
 * tags and also node_list tag. The result here should drop all concrete
 * `node:id` tags, as they are already covered by the main list one.
 *
 * Note that this is not limited for nodes or list tags only. Custom code can
 * consider other redundant scenarios and add them through implementing hook
 * `hook_cache_tags_simplify_dictionary` implementation.
 *
 * @see hook_cache_tags_simplify_dictionary()
 */
class CacheTagsSimplifySubscriber implements EventSubscriberInterface {

  /**
   * Static dictionary to use for simplification.
   *
   * @var string[]
   */
  protected $dictionary = [];

  /**
   * Class constructor.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->dictionary = $module_handler->invokeAll('cache_tags_simplify_dictionary');
    $module_handler->alter('cache_tags_simplify_dictionary', $this->dictionary);
  }

  /**
   * {@inheritDoc}
   *
   * @see \Drupal\purge\EventSubscriber\CacheableResponseSubscriber::getSubscribedEvents()
   */
  public static function getSubscribedEvents(): array {
    return [
      // To run before Purge's CacheableResponseSubscriber.
      KernelEvents::RESPONSE => ['onRespond', 1],
    ];
  }

  /**
   * Event "onRespond" handling.
   */
  public function onRespond(ResponseEvent $event): void {
    $response = $event->getResponse();

    if ($response instanceof CacheableResponseInterface) {
      $cache_tags = $response->getCacheableMetadata()->getCacheTags();

      foreach ($this->dictionary as $list_tag => $pattern) {
        if (\in_array($list_tag, $cache_tags, TRUE)) {
          foreach ($cache_tags as $key => $tag) {
            if (preg_match($pattern, $tag)) {
              unset($cache_tags[$key]);
            }
          }
        }
      }
      $response->getCacheableMetadata()->setCacheTags($cache_tags);
    }
  }

}
