<?php

/**
 * @file
 * Documentation of the Cache tags simplify module.
 */

/**
 * Supply additional dictionary for cache tags simplification.
 *
 * Cache tags should be simplified because most of webservers and HTTP proxies
 * have an upper limit on HTTP header length. You can supply extra entries into
 * the simplification dictionary to get even less cache tags.
 *
 * @return string[]
 *   Additional entries for the simplification dictionary. Keys should be a
 *   list cache tag whereas value should be a regex pattern matching all the
 *   cache tags corresponding to the list cache tag.
 */
function hook_cache_tags_simplify_dictionary(): array {
  // In this sample we would transform things like:
  // "node:1234, node_list, node:42" => "node_list".
  return [
    'node_list' => '/^node\:/',
  ];
}

/**
 * Alter the dictionary for cache tags simplification.
 *
 * @param string[] $dictionary
 *   Keys should be a list cache tag whereas value should be a regex pattern
 *   matching all the cache tags corresponding to the list cache tag.
 */
function hook_cache_tags_simplify_dictionary_alter(array &$dictionary): void {
  // In this sample we prevent the removing of "user:" cache tags when
  // 'user_list' is present.
  unset($dictionary['user_list']);
}
